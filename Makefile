TARGET=$(basename $(shell ls *.pro | head -n1))
all: $(TARGET).kicad_pcb
	scripts/kicad-fab.py $^ $(TARGET)_plot/ 
	git add $(TARGET)_plot/
	git commit -m "Updated fabrication files"

importscript:
	mkdir -p scripts/
	cp ~/bin/kicad-fab.py scripts 

.PHONY: all
